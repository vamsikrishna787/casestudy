import { render, screen } from '@testing-library/react';
import ByStop from './Bystop';
import userEvent from '@testing-library/user-event';


describe('By Stop Component',() => {
     test('testing if data loading on valid stop', async () => {
           
          //Asert
        render( <ByStop />);

        //Act
        userEvent.type(screen.getByRole('textbox'), '77')

         //Asert
        const findtarget = await screen.findByText(/Chicago Ave S & 58th St E/);
        expect(findtarget).toBeInTheDocument();
      });


      test('testing if not found was showing', async () => { 
                    
        //Asert
      render( <ByStop />);

      //Act
      userEvent.type(screen.getByRole('textbox'), '55')

       //Asert
      const findtarget = await screen.findByText(/Requested Stop Not Exist Please Enter Correct Stop!/);
      expect(findtarget).toBeInTheDocument();
    });

} )



