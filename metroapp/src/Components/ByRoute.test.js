import { render, screen } from '@testing-library/react';
import ByRoute from './Byroute';



describe('By Route Component',() => {
     test('render post if request succed', async () => {
               window.fetch = jest.fn()
               window.fetch.mockResolvedValueOnce(
               {
                  JSON:async() => [{direction_id:'77',direction_name:'southroute'}],
                  }
          );

        render( <ByRoute />);
        const listitemelement  = await screen.findAllByRole('option');
        expect(listitemelement).not.toHaveLength(0);
      });

} )



