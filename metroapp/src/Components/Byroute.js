import FormControl from '@material-ui/core/FormControl';
import Container from '@material-ui/core/Container';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import Grid from '@material-ui/core/Grid';
import React, { useState,useEffect  } from 'react';
import RouteGrid from './RouteGrid';


function  Byroute () {




  const [error, setError] = useState(null);
  const [isLoaded, setIsLoaded] = useState(false);
  


  const [routevalue, setroutevalue] = useState('');
  const [directionvalue, setdirectionvalue] = useState('');
  const [stopvalue, setstopvalue] = useState('');

  const [routeslist, setrouteslist] = useState([]);
  const [directionslist, setdirectionlist] = useState([]);
  const [stoplist, setstoplist] = useState([]);

  const [showgrid, setshowgrid] = useState(false);



  useEffect(() => {

      fetch("https://svc.metrotransit.org/nextripv2/routes")
      .then((response) => {
        if(!response.ok) throw new Error(response.status);
        else return response.json();
      })
      .then((data) => {
        setIsLoaded(true);
        setrouteslist(data);
      
      })
      .catch((error) => {
        setIsLoaded(true);
        setError(error);
     
      });
  }, [])





   function handleChange(e) {
    setroutevalue(e.target.value);
    setdirectionvalue("");
    setstopvalue("");
    setshowgrid(false);
    if(e.target.value ==="")
        {
          setshowgrid(false);     
          return;}
  

      fetch("https://svc.metrotransit.org/nextripv2/directions/"+e.target.value)
      .then((response) => {
        if(!response.ok) throw new Error(response.status);
        else return response.json();
      })
      .then((data) => {
        setIsLoaded(true);
        setdirectionlist(data);
      
      })
      .catch((error) => {
        setIsLoaded(true);
        setError(error);
     
      });


  }





  function handledirection(e) {

    setdirectionvalue(e.target.value);
    setstopvalue("");
    setshowgrid(false);
    if(e.target.value ==="")
    {
         return;
    }
  
      fetch("https://svc.metrotransit.org/nextripv2/stops/"+routevalue+"/"+e.target.value)
      .then((response) => {
        if(!response.ok) throw new Error(response.status);
        else return response.json();
      })
      .then((data) => {
        setIsLoaded(true);
        setstoplist(data);
      
      })
      .catch((error) => {
        setIsLoaded(true);
        setError(error);
     
      });


  }



  function handlestops(e){
  setstopvalue(e.target.value);
  if(e.target.value ==="")
  {setshowgrid(false);return;}
  setshowgrid(true);
   }






     return<>
    

    <Container maxWidth="sm" >  
        <Grid container spacing={3}>
        
        
    
  <Grid item xs={12}>
       <FormControl variant="outlined"  fullWidth>
        <InputLabel >Route</InputLabel>
        
         <Select native label="route"  onChange={handleChange}  id="routenumber" value={routevalue}>   
          <option aria-label="None" value="" />
          {routeslist.map((item,index) => (
            <option key={index} value={item.route_id}>{item.route_label}</option>
            ))}
         </Select>

      </FormControl>
  </Grid>


  {routevalue!== "" &&

  <Grid item xs={12}>
       <FormControl variant="outlined"  fullWidth>
        <InputLabel >Direction</InputLabel>
         <Select  native label="direction"  onChange={handledirection}  value={directionvalue} >   
          <option aria-label="None" value="" />

          {directionslist.map((item,index) => (
            <option key={index} value={item.direction_id}>{item.direction_name}</option>
            ))}

         </Select>
      </FormControl>
   </Grid>
 }

   

{routevalue!== "" && directionvalue!== "" &&
      <Grid item xs={12}> 
        <FormControl variant="outlined"  fullWidth>
        <InputLabel >Stop</InputLabel>
         <Select native label="Stop"  onChange={handlestops} value={stopvalue}>
          <option aria-label="None" value="" />

          {stoplist.map((item,index) => (
            <option key={index} value={item.place_code}>{item.description}</option>
            ))}

         </Select>
        </FormControl>
      </Grid>
    }

       


        </Grid> 
  </Container>


{showgrid=== true &&  <RouteGrid routeurl={"https://svc.metrotransit.org/nextripv2/"+routevalue+"/"+directionvalue+"/"+stopvalue}/>}

 </>
};
    
export default Byroute;

