import { render, screen } from '@testing-library/react';
import MainHeader from './MainHeader';
import userEvent from '@testing-library/user-event';

describe('Mian Header Component',() => {

    test('render heading', () => {
        render( <MainHeader />);
        const byrouteelement = screen.getByText(/Real-time Departures/);
        expect(byrouteelement).toBeInTheDocument();
      });


  
    test('render byroute tab', () => {
        render( <MainHeader />);
        const byrouteelement = screen.getByText('BY ROUTE',{exact:false});
        expect(byrouteelement).toBeInTheDocument();
      });


   test('render bystop tab', () => {
        render( <MainHeader />);
        const byrouteelement = screen.getByText('By Stop',{exact:false});
        expect(byrouteelement).toBeInTheDocument();
      });

   test('clicking on stoptab', () => {
        //arrange
        render( <MainHeader />);

        //act
        const byrouteelement = screen.getByText('By Stop',{exact:false});
        userEvent.click(byrouteelement);
        const textboxexist = screen.getByRole('textbox');
          
        //asert
        expect(textboxexist).toBeInTheDocument();
      });


    test('clicking on route tab', () => {
        //arrange
        render( <MainHeader />);

        //act
          const byrouteelement = screen.getByText('By Route',{exact:false});
          userEvent.click(byrouteelement);
          const textboxexist = screen.queryByRole('textbox');

        //asert
        expect(textboxexist).toBeNull();
      });


} )



