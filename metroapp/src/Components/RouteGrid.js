import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import React, { useState,useEffect  } from 'react';
import Container from '@material-ui/core/Container';
import { Alert, AlertTitle } from '@material-ui/lab';
import Typography from '@material-ui/core/Typography';
import CommuteIcon from '@material-ui/icons/Commute';
import RoomIcon from '@material-ui/icons/Room';

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});




const  RouteGrid = (props) => {

    const classes = useStyles();


    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);

    const [outputgridlist, setoutputgridlist] = useState([]);
    const [outputheaderlist, setoutputheaderlist] = useState([]);
   

  useEffect(() => {
  

      fetch(props.routeurl)
      .then((response) => {
        if(!response.ok) throw new Error(response.status);
        else return response.json();
      })
      .then((data) => {


        setoutputgridlist(data.departures);
         setoutputheaderlist(data.stops); 
         setError(null);
         setIsLoaded(true);     

      })
      .catch((error) => {
        setIsLoaded(true);
        setError(error);
     
      });
    
   
  }, [props])


  let  content;
  if (error !== null) 
  {
    content =<>
    <Container maxWidth="sm" >    
          <Alert severity="error">
          <AlertTitle><strong>Eroor</strong></AlertTitle>
          <strong>Requested Stop Not Exist Please Enter Correct Stop!</strong> 
        </Alert>
  </Container>
    
    </> ;
  }

  else if(outputgridlist.length === 0)
  {
    content =<>
    <Container maxWidth="sm" >    
          <Alert severity="warning">
          <AlertTitle><strong>Warning</strong></AlertTitle>
          <strong>No records Found</strong> 
        </Alert>
  </Container>
    
    </> ;
  }
   else 
    {
    content = <>
      <TableContainer component={Paper} style={{backgroundColor:'#f5f5f4'}}>
      <Table className={classes.table} aria-label="simple table">
        <TableHead>

          {outputheaderlist.map((item,index) => (
            <TableRow key={index}>
              <TableCell scope="row">  <Typography variant="h5" gutterBottom>{item.description} <CommuteIcon/> </Typography>     </TableCell>
              <TableCell></TableCell>
              <TableCell align="right"> <Typography variant="h5" gutterBottom>Stop #: {item.stop_id}<RoomIcon/></Typography></TableCell>       
            </TableRow>
          ))}

          <TableRow  style={{backgroundColor:'#ffd200'}}>
            <TableCell ><strong>ROUTE</strong> </TableCell>
            <TableCell ><strong>Destination</strong></TableCell>
            <TableCell align="right"><strong>Departs</strong>&nbsp;</TableCell>
           
          </TableRow>
        </TableHead>
        <TableBody>
          {outputgridlist.map((item,index) => (
            <TableRow key={index}>
              <TableCell >  <Typography variant="subtitle1" gutterBottom><strong>{item.route_short_name}</strong> </Typography> </TableCell>
              <TableCell ><Typography variant="subtitle1" gutterBottom>{item.description}</Typography></TableCell>
              
              <TableCell align="right" style={{color: item.schedule_relationship ==='Skipped' ? 'red':'black'}}>  <Typography variant="subtitle1" gutterBottom><strong>{item.departure_text} {item.terminal ? item.terminal : null }</strong> </Typography>     </TableCell>       
            </TableRow>
          ))}

        </TableBody>
      </Table>
    </TableContainer>
    </> ;
  }


  return <>

<br/>
<br/>

{content} 
  </>;
}
export default RouteGrid;