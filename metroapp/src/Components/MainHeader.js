
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Container from '@material-ui/core/Container';
import Place from '@material-ui/icons/Place';
import Directions from '@material-ui/icons/Directions';
import Typography from '@material-ui/core/Typography';
import {useParams,useHistory} from 'react-router-dom';
import React, { useState,useEffect  } from 'react';

import Bystop from './Bystop';
import Byroute from './Byroute';

const useStyles = makeStyles({
  root: {
    flexGrow: 1,
    maxWidth: 500,
  },
});

export default function MainHeader() {

const parms = useParams();
const history = useHistory();

let searchtype;



if(parms.type==="0")
{
  searchtype=0;
}

else if(parms.type==="1")
{
  searchtype=1;
}
else
{
  searchtype=0;
}


  const classes = useStyles();
  const [value, setValue] = useState(searchtype);



    useEffect(() => {
     setValue(searchtype);
    }, [searchtype])


  const handleChange = (event, newValue) => {
    history.push("/"+newValue);
    setValue(newValue);
  };


  let  content;
  if (value === 0) {
    content = <Byroute/>;
  } else {
    content =  <Bystop  stopid={parms.stopid}/> ;
  }


  return<> 
    <Container maxWidth="sm">

  <Typography component="h1" variant="h5" align="center" color="textPrimary" gutterBottom>
        Real-time Departures
      </Typography>
      {/* <Typography variant="h5" align="center" color="textSecondary" paragraph>
        test
      </Typography> */}



    <Paper square className={classes.root}>
      <Tabs value={value} onChange={handleChange} variant="fullWidth" indicatorColor="primary" textColor="primary" aria-label="icon label tabs example">
        <Tab icon={<Directions />} label="By Route" />
        <Tab icon={<Place />} label="By Stop" />
      </Tabs>
    </Paper>
    <br/><br/> 
          


            
    </Container>
  
    <Container maxWidth="md">

    {content} 
    </Container>



    </>
}
