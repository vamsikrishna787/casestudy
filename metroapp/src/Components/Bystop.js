import TextField from '@material-ui/core/TextField';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import RouteGrid from './RouteGrid';
import {useHistory} from 'react-router-dom';
import React, { useState,useEffect  } from 'react';

function  Bystop (props) {
     const history = useHistory();

let stopnumber;
let conditionalshowgrid= false;

if(props.stopid === undefined)
{
     stopnumber = "";
}
else
{
     stopnumber = props.stopid;
     conditionalshowgrid = true;
}


     const [stopvalue, setstopvalue] = useState(stopnumber);
     const [showgrid, setshowgrid] = useState(conditionalshowgrid);


     useEffect(() => {
          setstopvalue(stopnumber);
         }, [stopnumber])
     



     function handlestops(e){

          setstopvalue(e.target.value);
          history.push("/1/"+ e.target.value);
          if(e.target.value ==="") 
              {
                   setshowgrid(false);return;
          }
              setshowgrid(true);
     }



    return<>

   <Container maxWidth="sm" >  
        <Grid container spacing={3}>
        <Grid item xs={12}>
             <TextField id="outlined-basic" label="Enter stop number" value={stopvalue} onChange={handlestops}  fullWidth variant="outlined" />
        </Grid>
        </Grid> 
  </Container>


     {showgrid=== true &&  <RouteGrid routeurl={"https://svc.metrotransit.org/nextripv2/"+stopnumber}/>}

    </>
    };
    
    
    export default Bystop;