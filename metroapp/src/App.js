import AppBar from '@material-ui/core/AppBar';
import Train from '@material-ui/icons/Train';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import {Route,Switch} from 'react-router-dom';
import MainHeader from './Components/MainHeader';
import Notfound from './Components/Notfound';






function App() {
     return<>

     <AppBar position="relative">
        <Toolbar>
          <Train />
          <Typography variant="h6" color="inherit" noWrap>
           Metro Transit
          </Typography>
        </Toolbar>
      </AppBar>
     
     
 
        <br/> <br/>
     


        <Switch>

               <Route path = "/" exact>
                  <MainHeader></MainHeader>
               </Route>

               <Route path = "/:type" exact>
                  <MainHeader></MainHeader>
               </Route>

               <Route path = "/:type/:stopid" exact>
                  <MainHeader></MainHeader>
               </Route>



               <Route path = "*" >
                     <Notfound></Notfound>
               </Route>
        </Switch>



   <br/> <br/>


      
       

</>
};

export default App;

  
      


   
  
